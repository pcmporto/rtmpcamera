package com.voutestando.android.rtmp.broadcast.camera;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    private final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private boolean manualRequestPermission = false;
    private LinearLayout welcomeMessage;
    private TextView requestPermissionsMessage;
    private Button requestPermissionsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        requestPermissionsButton = findViewById(R.id.request_permissions_button);
        requestPermissionsButton.setOnClickListener(v -> requestRequiredPermissions());
        requestPermissionsButton.setVisibility(View.GONE);

        welcomeMessage = findViewById(R.id.welcome_message_linearLayout);

        requestPermissionsMessage = findViewById(R.id.request_permissions_message);
        requestPermissionsMessage.setVisibility(View.GONE);

        Handler handle = new Handler();
        handle.postDelayed(() -> requestOnStartRequiredPermissions(), 3000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (hasRequiredPermissions()) {
            startMainActivity();
        } else {
            manualRequestPermission = true;
            requestPermissionsButton.setEnabled(true);
            requestPermissionsButton.setVisibility(View.VISIBLE);
            requestPermissionsMessage.setVisibility(View.VISIBLE);
            welcomeMessage.setVisibility(View.GONE);
        }
    }

    private void requestOnStartRequiredPermissions() {
        if (manualRequestPermission) {
            return;
        }
        requestRequiredPermissions();
    }

    private void requestRequiredPermissions() {
        if (!hasRequiredPermissions()) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA},
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        } else {
            startMainActivity();
        }
    }

    private boolean hasRequiredPermissions() {
        return checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void startMainActivity() {
        startActivity(new Intent(getApplicationContext(), CameraPreferencesActivity.class));
        finish();
    }
}
