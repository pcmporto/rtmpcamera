package com.voutestando.android.rtmp.broadcast.camera.preferences.camera;

import android.widget.TextView;

import com.voutestando.android.rtmp.broadcast.camera.enums.CameraResolutionEnum;

import java.util.Map;

public class CameraPreferencesVO {

    public final static int LANDSCAPE_ROTATION = 0;
    public final static int PORTRAIT_ROTATION = 90;

    private CameraResolutionEnum resolution;
    private boolean portrait;
    private int fps;
    private String url;

    public CameraPreferencesVO(CameraResolutionEnum resolution, boolean portrait, int fps, String url) {
        this.resolution = resolution;
        this.portrait = portrait;
        this.fps = fps;
        this.url = url;
    }

    public CameraPreferencesVO(Map<String, ?> cameraRawPreferences) {
        this.resolution = CameraResolutionEnum.valueOf((String) cameraRawPreferences.get(CameraPreferences.RESOLUTION));
        this.portrait = (Boolean) cameraRawPreferences.get(CameraPreferences.PORTRAIT);
        this.fps = (Integer) cameraRawPreferences.get(CameraPreferences.FPS);
        this.url = (String) cameraRawPreferences.get(CameraPreferences.URL);
    }

    public int getWidth() {
        //return portrait ? resolution.getHeight() : resolution.getWidth();
        return resolution.getWidth();
    }

    public int getHeight() {
        //return portrait ? resolution.getWidth() : resolution.getHeight();
        return resolution.getHeight();
    }

    public int getRotation() {
        return portrait ? PORTRAIT_ROTATION : LANDSCAPE_ROTATION;
    }

    public CameraResolutionEnum getResolution() {
        return resolution;
    }

    public boolean isPortrait() {
        return portrait;
    }

    public int getFps() {
        return fps;
    }

    public String getUrl() {
        return url;
    }
}
