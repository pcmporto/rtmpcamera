package com.voutestando.android.rtmp.broadcast.camera.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum CameraResolutionEnum {
    P144(256, 144),
    P360(640, 360),
    P720(1280, 720),
    P1080(1920, 1080);
    private int width;
    private int height;

    CameraResolutionEnum(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public static List<String> getResolutionList() {
        List<String> res = new ArrayList<>();
        Arrays.stream(CameraResolutionEnum.values()).forEach(item -> res.add(item.name()));
        return res;
    }
}
