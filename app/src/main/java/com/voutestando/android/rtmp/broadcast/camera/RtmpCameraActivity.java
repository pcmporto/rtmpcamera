package com.voutestando.android.rtmp.broadcast.camera;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.pedro.encoder.input.video.CameraHelper;
import com.pedro.encoder.input.video.CameraOpenException;
import com.pedro.rtplibrary.rtmp.RtmpCamera1;
import com.pedro.rtplibrary.view.OpenGlView;
import com.voutestando.android.rtmp.broadcast.camera.preferences.camera.CameraPreferences;
import com.voutestando.android.rtmp.broadcast.camera.preferences.camera.CameraPreferencesVO;

import net.ossrs.rtmp.ConnectCheckerRtmp;

import androidx.appcompat.app.AppCompatActivity;

public class RtmpCameraActivity extends AppCompatActivity implements ConnectCheckerRtmp, View.OnClickListener, SurfaceHolder.Callback {

    private CameraPreferencesVO cPrefs;
    private RtmpCamera1 rtmpCamera;
    private Button startStopButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cPrefs = new CameraPreferences(this).getCameraPreferences();
        setRequestedOrientation(cPrefs.isPortrait() ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT : ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_rtmp_camera);
        OpenGlView openGlView = findViewById(R.id.openGlView);
        startStopButton = findViewById(R.id.b_start_stop);
        startStopButton.setOnClickListener(this);
        Button switchCamera = findViewById(R.id.switch_camera);
        switchCamera.setOnClickListener(this);
        rtmpCamera = new RtmpCamera1(openGlView, this);
        rtmpCamera.setReTries(10);
        openGlView.getHolder().addCallback(this);
    }

    @Override
    public void onConnectionSuccessRtmp() {
        runOnUiThread(() -> Toast.makeText(RtmpCameraActivity.this, "Connection success", Toast.LENGTH_SHORT).show());
    }

    @Override
    public void onConnectionFailedRtmp(final String reason) {
        runOnUiThread(() -> {
            if (rtmpCamera.reTry(5000, reason)) {
                Toast.makeText(RtmpCameraActivity.this, "Retry", Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(RtmpCameraActivity.this, "Connection failed. " + reason, Toast.LENGTH_SHORT)
                        .show();
                rtmpCamera.stopStream();
                startStopButton.setText(R.string.start_button);
            }
        });
    }

    @Override
    public void onNewBitrateRtmp(long bitrate) {

    }

    @Override
    public void onDisconnectRtmp() {
        runOnUiThread(() -> {
            Toast.makeText(RtmpCameraActivity.this, "Disconnected", Toast.LENGTH_SHORT).show();
            startStopButton.setText(R.string.start_button);
        });
    }

    @Override
    public void onAuthErrorRtmp() {
        runOnUiThread(() -> Toast.makeText(RtmpCameraActivity.this, "Auth error", Toast.LENGTH_SHORT).show());
    }

    @Override
    public void onAuthSuccessRtmp() {
        runOnUiThread(() -> Toast.makeText(RtmpCameraActivity.this, "Auth success", Toast.LENGTH_SHORT).show());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_start_stop:
                if (!rtmpCamera.isStreaming()) {
                    if (rtmpCamera.isRecording()
                            || rtmpCamera.prepareAudio() && rtmpCamera.prepareVideo(cPrefs.getWidth(), cPrefs.getHeight(), cPrefs.getFps(), 1200 * 1024, cPrefs.getRotation())) {
                        startStopButton.setText(R.string.stop_button);
                        rtmpCamera.startStream(cPrefs.getUrl());
                    } else {
                        Toast.makeText(this, "Error preparing stream, This device cant do it",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    startStopButton.setText(R.string.start_button);
                    rtmpCamera.stopStream();
                }
                break;
            case R.id.switch_camera:
                try {
                    rtmpCamera.switchCamera();
                } catch (CameraOpenException e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        rtmpCamera.startPreview(CameraHelper.Facing.BACK, cPrefs.getWidth(), cPrefs.getHeight(), cPrefs.getRotation());
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (rtmpCamera.isStreaming()) {
            rtmpCamera.stopStream();
            startStopButton.setText(getResources().getString(R.string.start_button));
        }
        rtmpCamera.stopPreview();
    }
}