package com.voutestando.android.rtmp.broadcast.camera.preferences.camera;

import android.graphics.Camera;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CameraUtil {
    private final List<String> fps;

    public CameraUtil() {
        fps = new ArrayList<>();
        fps.add("15");
        fps.add("24");
        fps.add("30");
        fps.add("60");
    }

    public List<String> getAvaliableFps() {
        return fps;
    }

    public int getIndexByValue(int value) {
        return fps.indexOf(String.valueOf(value));
    }
}
