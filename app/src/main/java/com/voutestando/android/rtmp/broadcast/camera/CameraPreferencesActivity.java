package com.voutestando.android.rtmp.broadcast.camera;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.voutestando.android.rtmp.broadcast.camera.enums.CameraResolutionEnum;
import com.voutestando.android.rtmp.broadcast.camera.preferences.camera.CameraPreferences;
import com.voutestando.android.rtmp.broadcast.camera.preferences.camera.CameraPreferencesVO;
import com.voutestando.android.rtmp.broadcast.camera.preferences.camera.CameraUtil;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class CameraPreferencesActivity extends AppCompatActivity implements View.OnClickListener {

    private CameraUtil cameraUtil = new CameraUtil();
    private CameraPreferences cameraPreferences;
    private Spinner resolutionSpinner;
    private Spinner fpsSpinner;
    private Switch portraitSwitch;
    private EditText inputUrl;
    private FloatingActionButton startCameraButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cameraPreferences = new CameraPreferences(this);
        setContentView(R.layout.activity_camera_preferences);

        CameraPreferencesVO cameraPreferencesVO = cameraPreferences.getCameraPreferences();

        initResolutionSpinner(cameraPreferencesVO);
        initFpsSpinner(cameraPreferencesVO);

        portraitSwitch = findViewById(R.id.portrait_switch);
        portraitSwitch.setChecked(cameraPreferencesVO.isPortrait());

        inputUrl = findViewById(R.id.url_editText);
        inputUrl.setText(cameraPreferencesVO.getUrl());

        startCameraButton = findViewById(R.id.start_button);
        startCameraButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.save();
    }

    public void save() {
        CameraPreferencesVO vo = new CameraPreferencesVO(
                CameraResolutionEnum.valueOf(resolutionSpinner.getSelectedItem().toString()),
                portraitSwitch.isChecked(),
                Integer.parseInt(fpsSpinner.getSelectedItem().toString()),
                inputUrl.getText().toString()
        );

        cameraPreferences.setCameraPreferences(vo);
        startActivity(new Intent(getApplicationContext(), RtmpCameraActivity.class));
    }

    private void initResolutionSpinner(CameraPreferencesVO vo) {
        resolutionSpinner = findViewById(R.id.resolution_spinner);
        resolutionSpinner.setAdapter(this.getResolutionSpinnerAdapter());
        resolutionSpinner.setSelection(vo.getResolution().ordinal());
    }

    private void initFpsSpinner(CameraPreferencesVO vo) {
        fpsSpinner = findViewById(R.id.fps_spinner);
        fpsSpinner.setAdapter(this.getFpsSpinnerAdapter());
        fpsSpinner.setSelection(cameraUtil.getIndexByValue(vo.getFps()));
    }

    private ArrayAdapter<String> getResolutionSpinnerAdapter() {
        return new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, CameraResolutionEnum.getResolutionList());
    }

    private ArrayAdapter<String> getFpsSpinnerAdapter() {
        return new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cameraUtil.getAvaliableFps());
    }
}
