package com.voutestando.android.rtmp.broadcast.camera.preferences.camera;

import android.content.Context;
import android.content.SharedPreferences;

import com.voutestando.android.rtmp.broadcast.camera.enums.CameraResolutionEnum;

public class CameraPreferences {

    public final static String RESOLUTION = "resolution";
    public final static String PORTRAIT = "portrait";
    public final static String FPS = "fps";
    public final static String URL = "url";

    private final static String PREFERENCES_NAME = "VT_BCC_RTMP_CAMERA_CONFIG";

    private final SharedPreferences sharedPreferences;

    public CameraPreferences(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public CameraPreferencesVO getCameraPreferences() {
        initCameraSharedPreferences();
        return new CameraPreferencesVO(this.sharedPreferences.getAll());
    }

    public void setCameraPreferences(CameraPreferencesVO vo) {
        this.saveCameraPreferences(vo.getResolution(), vo.isPortrait(), vo.getFps(), vo.getUrl());
    }

    private void initCameraSharedPreferences() {
        if (this.sharedPreferences.getAll().isEmpty()) {
            this.saveCameraPreferences(CameraResolutionEnum.P360, Boolean.FALSE, 15, "rtmp://example.com:1935");
        }
    }

    private void saveCameraPreferences(CameraResolutionEnum resolution, boolean portrait, int fps, String url) {
        this.sharedPreferences.edit()
                .putString(RESOLUTION, resolution.name())
                .putBoolean(PORTRAIT, portrait)
                .putInt(FPS, fps)
                .putString(URL, url)
                .apply();
    }


}
